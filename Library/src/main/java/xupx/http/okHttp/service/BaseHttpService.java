package xupx.http.okHttp.service;

import xupx.http.okHttp.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by xupx on 2017/3/29.
 * 服务基类
 */
public class BaseHttpService {

    /**
     * 请求 对象
     */
    protected RequestParam getRequestParam(String url, Map<String, Object> params) {
        return getRequestParam(url, params, RequestParam.METHOD.POST);
    }

    /**
     * 请求 对象
     */
    protected RequestParam getRequestParam(String url, Map<String, Object> params, RequestParam.METHOD method) {
        return new RequestParam(url, params, method);
    }

    /**
     * 获取返回类型
     */
    protected ResultType getResultType(ResultType.Type resultType, Class resultClass) {
        return new ResultType(resultType, resultClass);
    }

    /**
     * 获取缓存时间
     */
    protected CacheTime getCacheTime(TimeUnit timeUnit, int time) {
        return new CacheTime(timeUnit, time);
    }

    /**
     * 执行 请求
     */
    protected void executeRequest(RequestParam param, OnResponseListener listener) {
        executeRequest(param, null, listener);
    }

    /**
     * 执行 请求
     */
    protected void executeRequest(RequestParam param, ResultType resultType, OnResponseListener listener) {
        executeRequest(param, resultType, null, listener);
    }

    /**
     * 执行 请求
     */
    protected void executeRequest(RequestParam param, ResultType resultType, CacheTime cacheTime, OnResponseListener listener) {
        HttpClient.getInstance().request(param, resultType, cacheTime, listener);
    }

    /**
     * 生成默认参数
     */
    protected Map<String, Object> getMap() {
        Map<String, Object> params = new HashMap<>();
        return params;
    }

}
