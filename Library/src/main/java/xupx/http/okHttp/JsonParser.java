package xupx.http.okHttp;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.util.List;
import java.util.Map;

/**
 * Created by xupx on 2017/3/29.
 * 使用fastjson 解析json字符串
 */
public class JsonParser {

    /**
     * 将json转换成为java对象
     */
    public static <T> T json2Object(String str, Class<T> classOfT) {
        if (null == str) {
            return null;
        }
        try {
            return JSON.parseObject(str, classOfT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 将json转换成为java数组
     */
    public static <T> List<T> json2List(String json, Class<T> classOfT) {
        if (null == json) {
            return null;
        }
        try {
            return JSON.parseArray(json, classOfT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * json 转换 map
     */
    public static Map<String, Object> json2Map(String json) {
        if (null == json) {
            return null;
        }
        try {
            return JSONObject.parseObject(json);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
