package xupx.http.okHttp;

import okhttp3.*;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * Created by xupx on 2017/3/29.
 * http 请求入口
 */
public class HttpClient {

    public static String cachePath = null;// 缓存路径
    public static int cacheSize = 1024 * 1024 * 10;// 缓存大小
    public static boolean clearCache = false;// 是否清除缓存
    public static int connectTimeout = 8;// unit second 连接超时时间
    public static int writeTimeout = 8;// unit second 写超时时间
    public static int readTimeout = 8;// unit second 读超时时间
    public static boolean retryOnConnectionFailure = false;// 连接失败是否重试

    private static HttpClient instance;

    public static HttpClient getInstance() {
        if (instance == null) {
            instance = new HttpClient();
        }
        return instance;
    }

    // okHttpClient
    private OkHttpClient okHttpClient;

    private HttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(connectTimeout, java.util.concurrent.TimeUnit.SECONDS);  //设置连接超时时间
        builder.writeTimeout(writeTimeout, java.util.concurrent.TimeUnit.SECONDS);//设置写入超时时间
        builder.readTimeout(readTimeout, java.util.concurrent.TimeUnit.SECONDS);//设置读取数据超时时间
        builder.retryOnConnectionFailure(retryOnConnectionFailure);//设置不进行连接失败重试
        // 设置缓存规则
        if (cachePath != null) {
            Cache cache = new Cache(new File(cachePath), cacheSize);
            builder.cache(cache); // 这种缓存
            // 是否清除缓存
            if (clearCache) {
                try {
                    cache.delete();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        okHttpClient = builder.build();
    }

    /**
     * 设置请求参数
     */
    private void setParams(Request.Builder builder, RequestParam param) {
        // 设置 url
        builder.url(param.url);
        // 设置参数
        if (param.params != null && param.params.size() > 0) {
            FormBody.Builder formBuild = new FormBody.Builder();
            for (Map.Entry<String, Object> entry : param.params.entrySet()) {
                if (param.method == RequestParam.METHOD.POST) {
                    // post
                    formBuild.add(entry.getKey(), String.valueOf(entry.getValue()));
                } else {
                    // get
                    formBuild.addEncoded(entry.getKey(), String.valueOf(entry.getValue()));
                }
            }
            RequestBody requestBody = formBuild.build();
            builder.method(param.method == RequestParam.METHOD.POST ? "POST" : "GET", requestBody);
        }
    }

    /**
     * 设置缓存
     */
    private void setCache(Request.Builder builder, CacheTime cacheTime) {
        // 设置请求 缓存
        if (cacheTime != null) {
            CacheControl.Builder cacheBuild = new CacheControl.Builder();
            cacheBuild.maxStale(cacheTime.staleTime, cacheTime.timeUnit);
            builder.cacheControl(cacheBuild.build());
        }
    }

    /**
     * 解析json
     */
    private void parseJson(Response response, ResultType resultType, final OnResponseListener responseListener) {
        // 在线程里运行
        try {
            String json = response.body().string();
            // 解析JSON
            ResultObject result = JsonParser.json2Object(json, ResultObject.class);
            if (result == null || result.getCode() != ResultObject.SUCCESS) {
                if (responseListener != null) {
                    responseListener.onFail();
                }
                return;
            }
            if (result.getData() != null && resultType.resultObjectType != ResultType.Type.NONE) {
                String jsonStr = result.getData().toString();
                Object o = null;
                if (resultType.resultObjectType == ResultType.Type.ARRAY) {
                    o = JsonParser.json2List(jsonStr, resultType.resultObjClass);
                } else if (resultType.resultObjectType == ResultType.Type.OBJECT) {
                    o = JsonParser.json2Object(jsonStr, resultType.resultObjClass);
                } else if (resultType.resultObjectType == ResultType.Type.SIMPLE) {
                    o = result.getData();
                }
                if (o == null) {
                    if (responseListener != null) {
                        responseListener.onFail();
                    }
                    return;
                }
                result.setData(o);
            }
            if (responseListener != null) {
                responseListener.onSuccess(result);
            }
        } catch (IOException e) {
            if (responseListener != null) {
                responseListener.onFail();
            }
        }
    }

    /**
     * 网络请求
     */
    public void request(RequestParam param, final ResultType resultType, CacheTime cacheTime, final OnResponseListener responseListener) {
        Request.Builder builder = new Request.Builder();
        // 设置请求参数
        setParams(builder, param);
        // 设置缓存
        setCache(builder, cacheTime);
        // 加入异步请求队列
        Call call = okHttpClient.newCall(builder.build());
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                if (responseListener != null) {
                    responseListener.onFail();
                }
            }

            @Override
            public void onResponse(Call call, Response response) {
                // 解析 json
                parseJson(response, resultType, responseListener);
            }
        });
    }

}
