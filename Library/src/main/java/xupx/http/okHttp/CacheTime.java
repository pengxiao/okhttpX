package xupx.http.okHttp;

import java.util.concurrent.TimeUnit;

/**
 * Created by xupx on 2017/3/29.
 * 缓存时间
 */
public class CacheTime {

    TimeUnit timeUnit;
    int staleTime;

    public CacheTime(TimeUnit timeUnit, int staleTime) {
        this.timeUnit = timeUnit;
        this.staleTime = staleTime;
    }

}
