package xupx.http.okHttp;

/**
 * Created by xupx on 2017/3/29.
 *
 */
public interface OnResponseListener {

    void onSuccess(ResultObject resultObject);
    void onFail();

}
