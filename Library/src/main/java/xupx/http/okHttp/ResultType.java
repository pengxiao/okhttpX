package xupx.http.okHttp;

/**
 * Created by xupx on 2017/3/29.
 * 返回类型
 */
public class ResultType {

    public enum Type{
        NONE, SIMPLE, OBJECT, ARRAY
    }

    Class resultObjClass;// 返回的对象
    Type resultObjectType;// 返回的类

    public ResultType() {
        resultObjectType = Type.NONE;
    }

    public ResultType(Type resultObjectType, Class resultObjClass) {
        this.resultObjectType = resultObjectType;
        this.resultObjClass = resultObjClass;
    }

    public Class getResultObjClass() {
        return resultObjClass;
    }

    public void setResultObjClass(Class resultObjClass) {
        this.resultObjClass = resultObjClass;
    }

    public Type getResultObjectType() {
        return resultObjectType;
    }

    public void setResultObjectType(Type resultObjectType) {
        this.resultObjectType = resultObjectType;
    }
}
