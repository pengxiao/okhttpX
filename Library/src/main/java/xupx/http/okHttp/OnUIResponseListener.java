package xupx.http.okHttp;

import android.app.Activity;

/**
 * Created by xupx on 2017/3/29.
 * 在UI线程里执行的监听器
 */
public abstract class OnUIResponseListener implements OnResponseListener {

    private Activity context;
    private DestroyInterface destroyInterface;

    public OnUIResponseListener(Activity activity, DestroyInterface destroyInterface) {
        this.context = activity;
        this.destroyInterface = destroyInterface;
    }

    @Override
    public final void onSuccess(final ResultObject resultObject) {
        if (context != null && destroyInterface != null && !destroyInterface.isDestroy()) {
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    callUISuccess(resultObject);
                }
            });
        }
        // 执行耗时操作
        doSomethingInThread(resultObject);
    }

    @Override
    public final void onFail() {
        if (context != null && destroyInterface != null && !destroyInterface.isDestroy()) {
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    callUIFail();
                }
            });
        }
    }

    /**
     * 在线程里运行，耗时操作
     */
    public void doSomethingInThread(ResultObject resultObject) {
    }

    /**
     * 成功回调，运行在UI线程
     */
    public abstract void callUISuccess(ResultObject resultObject);

    /**
     * 失败回调，运行在UI线程
     */
    public abstract void callUIFail();
}
