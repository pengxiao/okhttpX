package xupx.http.okHttp;

/**
 * Created by xupx on 2017/3/29.
 *
 */
public class ResultObject {

    public static final int SUCCESS = 0;

    private int code;
    private Object data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
