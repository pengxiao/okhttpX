package xupx.http.okHttp;

import java.util.Map;

/**
 * Created by xupx on 2017/3/29.
 *
 */
public class RequestParam {

    public enum METHOD {
        GET, POST
    }

    METHOD method;
    String url;
    Map<String, Object> params;

    public RequestParam(String url) {
        this(url, null);
    }

    public RequestParam(String url, Map<String, Object> params) {
        this(url, params, METHOD.POST);
    }

    public RequestParam(String url, Map<String, Object> params, METHOD method) {
        this.url = url;
        this.params = params;
        this.method = method;
    }

}
