package xupx.test.okHttp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;
import xupx.http.okHttp.DestroyInterface;
import xupx.http.okHttp.OnUIResponseListener;
import xupx.http.okHttp.ResultObject;

public class MainActivity extends AppCompatActivity implements DestroyInterface {

    private TextView tv_json;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv_json = (TextView) findViewById(R.id.tv_json);
        // 测试 okHttpGet
        testOkHttpGet();
    }

    /**
     * 测试 http
     */
    private void testOkHttpGet() {
        UserService.getInstance().getUserById(13, new OnUIResponseListener(this, this) {
            @Override
            public void callUISuccess(ResultObject resultObject) {
                UserBean user = (UserBean) resultObject.getData();
                tv_json.setText(user.getId() + " , " + user.getName());
            }

            @Override
            public void callUIFail() {
                Toast.makeText(getApplicationContext(), "失败", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        destroy = true;
        super.onDestroy();
    }

    private boolean destroy;

    @Override
    public boolean isDestroy() {
        return destroy;
    }
}
