package xupx.test.okHttp;

import xupx.http.okHttp.CacheTime;
import xupx.http.okHttp.OnResponseListener;
import xupx.http.okHttp.RequestParam;
import xupx.http.okHttp.ResultType;
import xupx.http.okHttp.service.BaseHttpService;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by xupx on 2017/3/29.
 *
 */
public class UserService extends BaseHttpService {

    private static UserService instance;

    private UserService() {
    }

    public static UserService getInstance() {
        if (instance == null) {
            instance = new UserService();
        }
        return instance;
    }

    private static final String getUserById = "http://192.168.1.165:8080/test/user";

    /**
     * 获取用户信息
     */
    public void getUserById(int userId, OnResponseListener listener) {
        Map<String, Object> map = getMap();
        map.put("uid", userId);
        RequestParam param = getRequestParam(getUserById, map);
        ResultType resultType = getResultType(ResultType.Type.OBJECT, UserBean.class);
        CacheTime cacheTime = getCacheTime(TimeUnit.SECONDS, 10);
        executeRequest(param, resultType, cacheTime, listener);
    }

}
