package xupx.test.okHttp;

import android.app.Application;
import android.os.Environment;
import xupx.http.okHttp.HttpClient;

import java.io.File;

/**
 * Created by xupx on 2017/3/29.
 *
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        // 缓存目录
        HttpClient.cachePath = getAppCacheDir() + File.separator + "httpCache";
    }

    /**
     * 获取缓存目录
     */
    public String getAppCacheDir(){
        String cachePath;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())
                || !Environment.isExternalStorageRemovable()) {
            cachePath = getExternalCacheDir().getPath();
        } else {
            cachePath = getCacheDir().getPath();
        }
        return cachePath;
    }
}
